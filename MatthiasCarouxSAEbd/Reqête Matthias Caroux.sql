-- TP 2_04
-- Nom:Caroux  , Prenom:Matthias 

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des objets vendus par ght1ordi au mois de février 2023

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+----------------------+
-- | pseudout | nomob                |
-- +----------+----------------------+
-- | etc...
-- = Reponse question 1.



select pseudoUt, nomob
from UTILISATEUR natural join OBJET natural join VENTE
where pseudoUt = "ght1ordi" and YEAR(finVe) = 2023 and MONTH(finVe) = 02;




-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des utilisateurs qui ont enchérit sur un objet qu’ils ont eux même mis en vente

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------+
-- | pseudout  |
-- +-----------+
-- | etc...
-- = Reponse question 2.


select pseudoUt, nomOb, prixBase, prixMin
from UTILISATEUR natural join OBJET natural join VENTE;
where exist in {
    select pseudoUt
    from UTILISATEUR natural join 
}

select nomOb, pseudoUt, montant
from OBJET natural join UTILISATEUR, ENCHERIR;

-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des utilisateurs qui ont mis en vente des objets mais uniquement des meubles

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------------+
-- | pseudout    |
-- +-------------+
-- | etc...
-- = Reponse question 3.

select pseudoUt
from UTILISATEUR natural join OBJET
where idCat = 3;


-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  La liste des objets qui ont généré plus de 15 enchères en 2022

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+----------------------+
-- | idob | nomob                |
-- +------+----------------------+
-- | etc...
-- = Reponse question 4.

select idob, nomob
from OBJET natural join VENTE natural join ENCHERIR
where YEAR(dateheure) = 2022 and idob in(
    select idob
    from ENCHERIR
    group by idob
    having count(dateheure) > 15
);


-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Ici NE CREEZ PAS la vue PRIXVENTE mais indiquer simplement la requête qui lui est associée. C'est à dire la requête permettant d'obtenir pour chaque vente validée, l'identifiant de la vente l'identiant de l'acheteur et le prix de la vente.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+------------+----------+
-- | idve | idacheteur | montant  |
-- +------+------------+----------+
-- | etc...
-- = Reponse question 5.

select idve, idut idacheteur, montant
from VENTE natural join ENCHERIR natural join UTILISATEUR natural join STATUT
where nomSt = 'Validée'
group by idve
having max(montant);

-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Le chiffre d’affaire par mois de la plateforme (en utilisant la vue PRIXVENTE)

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+-------+-----------+
-- | mois | annee | ca        |
-- +------+-------+-----------+
-- | etc...
-- = Reponse question 6.


create or replace view PRIXVENTE as(
    select MONTH(dateheure) mois, YEAR(dateheure) annee, SUM(montant) ca
    from ENCHERIR natural join VENTE natural join STATUT
    where nomSt = 'Validée'
    group by mois
);


-- +------------------+--
-- * Question 7 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations du ou des utilisateurs qui ont mis le plus d’objets en vente

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+----------+------+
-- | idut | pseudout | nbob |
-- +------+----------+------+
-- | etc...
-- = Reponse question 7.

select idut, pseudout, count(nomOb) nbob
from OBJET natural join UTILISATEUR
group by idUt
having nbob >= ALL(
    select count(nomOb)
    from OBJET natural join UTILISATEUR
    group by idut
);


-- +------------------+--
-- * Question 8 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  le camembert

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------+-------------------+-----------+
-- | idcat | nomcat            | nb_objets |
-- +-------+-------------------+-----------+
-- | etc...
-- = Reponse question 8.

select nomcat, count(idve) nb_objets
from OBJET natural join CATEGORIE natural join VENTE natural join STATUT
where nomSt = 'Validée' and YEAR(finVe) = 2022
group by nomcat;

-- +------------------+--
-- * Question 9 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Le top des vendeurs

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------+-------------+----------+
-- | idut | pseudout    | total    |
-- +------+-------------+----------+
-- | etc...
-- = Reponse question 9.


select pseudoUt , SUM(montant) total
from UTILISATEUR natural join ENCHERIR natural join VENTE
where finVe <= '2023-01-01'
group by pseudoUt
order by total DESC
LIMIT 10;







--Partie Voleur


--"L'objet a été mis en vente en debut de mois"
--"Il a été mis en vente à une somme inférieure à 500€."
--"Il a été vendu à plus de 10 fois le prix de vente initial"
--select nomOb, pseudoUt
--from OBJET natural join UTILISATEUR natural join VENTE
--where DAY(debutVe)<=15 and prixBase < 500 and nomOb in(
--    select nomOb
--    from OBJET natural join VENTE natural join ENCHERIR
--    where finVe = dateheure
--);



--Partie Math

--select count(idve), MONTH(finVe) mois
--from VENTE
--where YEAR(finVe) = 2022
--group by mois;

--select count(montant), MONTH(dateheure) mois
--from ENCHERIR
--where YEAR(dateheure) = 2022
--group by mois;
